import {
  Link,
  Outlet,
  ReactLocation,
  Router,
} from "react-location";

// Set up a ReactLocation instance
const location = new ReactLocation();

function getActiveProps() {
  return {
    style: {
      fontWeight: "bold",
    },
  };
}

export default function App() {
  return (
    // Build our routes and render our router
    <Router
      location={location}
      routes={[
        { path: "/", element: <Index /> },
        {
          path: "institutions",
          element: <InstitutionsIndex />,
          children: [
            {
              path: ":institutionId",
              element: <Institution />,
              children: [
                {
                  path: "students",
                  element: <StudentsIndex />,
                  children: [
                    {
                      path: ":studentId",
                      element: <Student />
                    }
                  ]
                }
              ]
            },
          ],
        },
      ]}
    >

      <div>
        <Link
          to="/"
          getActiveProps={getActiveProps}
          activeOptions={{ exact: true }}
        >
          Home
        </Link>{" "}
        <Link to="institutions" getActiveProps={getActiveProps}>
          Institutions
        </Link>
      </div>

      <hr />
      <Outlet /> {/* Start rendering router matches */}
    </Router>
  );
}
function Index() {
  return (
    <div>
      <h3>Welcome Home!</h3>
    </div>
  );
}

function InstitutionsIndex() {
  const institutions = [
    {
      id: 1,
      title: 'Institution One'
    }
  ];
  return (
    <div>
      <div>
        {institutions?.map((institution) => {
          return (
            <div key={institution.id}>
              <Link to={institution.id} getActiveProps={getActiveProps}>
                <pre>{institution.title}</pre>
              </Link>
            </div>
          );
        })}
      </div>
      <hr />
      <Outlet />
      <h1>Institution Index</h1>
      <div>Select an institution.</div>
    </div>
  );
}

function Institution() {
  return (
    <div>
      <div>
        <Link to="students" getActiveProps={getActiveProps}>
          <pre>Institution Students</pre>
        </Link>
      </div>
      <hr />
      <Outlet />
      <h1>Institution Body</h1>
    </div>
  );
}

function StudentsIndex() {
  const students = [{
    id: '12',
    name: 'Bruce Wayne'
  }]
  return (
    <div>
      <div>
        {students?.map((student) => {
          return (
            <div key={student.id}>
              <Link to={student.id} getActiveProps={getActiveProps}>
                <pre>{student.name}</pre>
              </Link>
            </div>
          );
        })}
      </div>
      <hr />
      <Outlet />
      <h1>Students Index</h1>
      <div>Select a student.</div>
    </div>
  );
}

function Student() {
  return (
    <div>
      <h1>Student Body</h1>
    </div>
  );
}